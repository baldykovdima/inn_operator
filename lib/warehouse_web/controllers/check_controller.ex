defmodule WarehouseWeb.CheckController do
  use WarehouseWeb, :controller

  alias Warehouse.Checks.CheckManager
  alias Warehouse.Locks.Lock

  action_fallback WarehouseWeb.FallbackController

  def index(conn, _params) do
    checks = CheckManager.list_by_date_desc()
    render(conn, "index.html", checks: checks)
  end

  def delete(conn, %{"id" => id}) do
    check = CheckManager.get_check!(id)

    case CheckManager.delete_check(check, conn.assigns.current_user) do
      {:ok, _check} ->
        conn
        |> put_flash(:info, "Check deleted successfully.")
        |> redirect(to: Routes.check_path(conn, :index))

      _ ->
        conn
        |> put_flash(:error, "Check deleted insuccessfully.")
        |> redirect(to: Routes.check_path(conn, :index))
    end
  end

  def block(conn, %{"id" => id}) do
    check = CheckManager.get_check!(id)

    case Lock.lock(check.ip, conn.assigns.current_user) do
      {:ok, _count} ->
        conn
        |> put_flash(:info, "IP blocked successfully.")
        |> redirect(to: Routes.check_path(conn, :index))

      _ ->
        conn
        |> put_flash(:error, "IP blocked insuccessfully.")
        |> redirect(to: Routes.check_path(conn, :index))
    end
  end
end
