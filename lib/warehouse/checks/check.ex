defmodule Warehouse.Checks.Check do
  use Ecto.Schema
  import Ecto.Changeset

  # @derive {Jason.Encoder, only: [:checkDate, :inn, :checkResult]}

  schema "checks" do
    field :checkDate, :naive_datetime
    field :checkResult, :boolean, default: false
    field :inn, :string
    field :ip, :string

    timestamps()
  end

  @doc false
  def changeset(check, attrs) do
    check
    |> cast(attrs, [:checkDate, :inn, :checkResult, :ip])
    |> validate_required([:checkDate, :inn, :checkResult, :ip])
  end
end
