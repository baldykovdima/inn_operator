defmodule Warehouse.Checks.Policy do
  @behaviour Bodyguard.Policy

  def authorize(:delete_check, user, _check), do: user.role == "operator" || user.role == "admin"

  def authorize(:operator, user, _check), do: user.role == "operator"
  def authorize(:admin, user, _check), do: user.role == "admin"
end
