defmodule Warehouse.Redis do
  @moduledoc """
  Модуль для работы с Reids
  """

  @doc """
  Подключается к Redis и возвращает соединение
  """
  @spec connect :: :ignore | {:error, any} | {:ok, pid}
  def connect() do
    Redix.start_link("redis://localhost:6379/3")
  end
end
