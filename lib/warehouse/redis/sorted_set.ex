defmodule Warehouse.Redis.SortedSet do
  @moduledoc """
  Модуль для работы с Sorted Set в Redis
  """

  @doc """
  Добавляет ключ в список
  Значение Score = времени добавления ключа в формате unix()
  """
  def add_item(conn, list, item) do
    range = DateTime.utc_now() |> DateTime.to_unix() |> Integer.to_string()
    Redix.command(conn, ["ZADD", list, range, item])
  end

  @doc """
  Удаляет ключи из списка по указанному за указанное количество секунд
  Набор удаляемых значений определяется по Score в котором значение равно времени добавления ключа в формате unix()
  """
  def remove_by_time(conn, list, time) do
    range =
      DateTime.utc_now()
      |> DateTime.add(-time, :second)
      |> DateTime.to_unix()
      |> Integer.to_string()

    Redix.command(conn, ["ZREMRANGEBYSCORE", list, "-inf", range])
  end
end
