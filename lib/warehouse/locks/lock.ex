defmodule Warehouse.Locks.Lock do
  @moduledoc """
  Модуль управления блокировками
  """
  use Task

  alias Warehouse.Redis
  alias Warehouse.Users.User

  def start_link(_arg) do
    {:ok, conn} = Redis.connect()
    Task.start_link(Warehouse.Locks.Lock, :remove_expired, [conn])
  end

  @doc """
  Удаляет ключ из списка блокировок старее заданного времени
  """
  def remove_expired(conn) do
    receive do
    after
      5_000 ->
        Redis.SortedSet.remove_by_time(conn, "locks", 30 * 60)
        remove_expired(conn)
    end
  end

  @doc """
  Добавляет ключ в список блокировок с текущим временем
  Если ключ уже есть в списке, обновляет значение времени на текущее
  """
  def lock(item, %User{} = user) do
    with :ok <- Bodyguard.permit(Warehouse.Checks.CheckManager, :admin, user, item) do
      {:ok, conn} = Redis.connect()
      Redis.SortedSet.add_item(conn, "locks", item)
    end
  end
end
