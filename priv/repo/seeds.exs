alias Warehouse.Repo
alias Warehouse.Users.User

# password 1234567890

Repo.insert!(%User{
  email: "admin",
  password_hash:
    "$pbkdf2-sha512$100000$fLVzvwBhtQED5STSQDz8bQ==$ZPUG4IjClLGaBIAMbGq/5w2R7XsF8WHJ50TTa/vQKGB/n6LFpJxhmK5XDnnLgz89PVTQ35jwD5NuLv7Viftmug==",
  email_confirmation_token: "c987154b-82ff-4fb0-bdab-ec7126448fcd",
  email_confirmed_at: DateTime.utc_now() |> DateTime.truncate(:second),
  role: "admin"
})

Repo.insert!(%User{
  email: "operator",
  password_hash:
    "$pbkdf2-sha512$100000$fLVzvwBhtQED5STSQDz8bQ==$ZPUG4IjClLGaBIAMbGq/5w2R7XsF8WHJ50TTa/vQKGB/n6LFpJxhmK5XDnnLgz89PVTQ35jwD5NuLv7Viftmug==",
  email_confirmation_token: "eaeff8b6-c200-400e-8043-420ef97fde6e",
  email_confirmed_at: DateTime.utc_now() |> DateTime.truncate(:second),
  role: "operator"
})
